const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  cityId: { type: String, default: null },
  stopName: { type: String, default: null }
});

module.exports = mongoose.model("stops", userSchema);
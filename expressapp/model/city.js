const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  cityName: { type: String, default: null }
});

module.exports = mongoose.model("city", userSchema);
'use strict';


const signIn = require('./signIn');
const city = require('./city');
const stops = require('./stops');

module.exports = [].concat(
    signIn,
    city,
    stops
);
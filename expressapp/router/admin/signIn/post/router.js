
const express = require("express");
const router = express.Router();
const signIn = require("./signIn");


router.post("/admin/signIn", signIn.signIn);


module.exports = router;

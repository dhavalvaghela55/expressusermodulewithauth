
const User = require("../../../../model/admin");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
exports.signIn = async (req, res) => {
    // Our login logic starts here
    try {
        // Get user input
        const { email, password } = req.body;

        // Validate user input
        if (!(email && password)) {
            res.status(400).send("All Signin input is required");
        }
        console.log("user---",User)
        // Validate if user exist in our database
        const user = await User.findOne({ email });
        console.log('user',user)
        if (user && (await bcrypt.compare(password, user.password))) {
            // Create token
            const token = jwt.sign(
                { user_id: user._id, email },
               'hhhsssmmmsss',
                {
                    expiresIn: "2h",
                }
            );

            // save user token
            user.token = token;

            // user
            res.status(200).send({ "data": user });
        } else {
            res.status(400).send("Invalid Credentials");
        }
    } catch (err) {
        console.log(err);
    }
    // Our register logic ends here
}
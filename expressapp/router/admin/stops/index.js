'use strict'


const post = require('./post');
const get = require('./get');
const deleteStop = require('./delete');

module.exports = [].concat(
    post,
    get,
    deleteStop
);
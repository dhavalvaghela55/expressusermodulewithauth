const express = require("express");
const router = express.Router();
const stop = require("./delete");
const auth = require("../../../../middleware/adminAuth");

router.delete("/admin/deleteStop", auth, stop.deleteStop);


module.exports = router;

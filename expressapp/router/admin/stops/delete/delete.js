
const Stop = require("../../../../model/stops");

exports.deleteStop = async (req, res) => {
    // Our login logic starts here
    try {
        // Get user input
        const { stopId } = req.body;

        // Validate user input
        if (!(stopId)) {
            res.status(400).send("All input is required");
        }

        // Validate if user exist in our database
        const stop = await Stop.findOneAndDelete({ _id:stopId });
        console.log('stop',stop)
        if (stop!==null) {
            res.status(200).send({ "data": stop });
        } else {
            res.status(400).send("stop not found");
        }
    } catch (err) {
        console.log(err);
    }
    // Our register logic ends here
}
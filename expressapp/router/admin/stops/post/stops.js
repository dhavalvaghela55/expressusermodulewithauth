
const Stop = require("../../../../model/stops");
const City = require("../../../../model/city");
exports.create = async (req, res) => {
    try {
      
        // Get user input
        const { stopName,cityId} = req.body;

        // Validate user input
        if (!(cityId && stopName)) {
            res.status(400).send("All input is required");
        }

        // check if city already exist
        const existCity = await City.findOne({ _id :cityId });
        console.log("existCity",existCity)
        const existStop = await Stop.findOne({ stopName,cityId });

        try{
            if (!existCity) {
                return res.status(409).send("City is Not Exist.");
            }
            if(existStop){
                return res.status(409).send("Stop is Already Exist in this city."); 
            }
        }catch(e){
            return res.status(409).send("City is Not Exist.");
        }

        const city = await Stop.create({
            cityId,
            stopName
        });

        // return new user
        res.status(201).json(city);
    } catch (err) {
        console.log(err);
    }
}
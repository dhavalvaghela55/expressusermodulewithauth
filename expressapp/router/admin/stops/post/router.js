
const express = require("express");
const router = express.Router();
const stops = require("./stops");

const auth = require("../../../../middleware/adminAuth");
router.post("/admin/addStopByCityId",auth, stops.create);


module.exports = router;

const express = require("express");
const router = express.Router();
const getStop = require("./getStopsByCityId");
const auth = require("../../../../middleware/adminAuth");

router.get("/admin/getStopsByCityId",auth, getStop.getStopByCityId)
module.exports = router;

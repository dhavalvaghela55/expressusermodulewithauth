const express = require("express");
const router = express.Router();
const getCity = require("./getCity");
const auth = require("../../../../middleware/adminAuth");

router.get("/admin/city",auth, getCity.getCity)
module.exports = router;

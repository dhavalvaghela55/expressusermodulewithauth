
const express = require("express");
const router = express.Router();
const city = require("./city");

const auth = require("../../../../middleware/adminAuth");
router.post("/admin/city",auth, city.create);


module.exports = router;

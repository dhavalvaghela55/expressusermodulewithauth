
const City = require("../../../../model/city");
exports.create = async (req, res) => {
    try {
      
        // Get user input
        const { cityName} = req.body;

        // Validate user input
        if (!(cityName)) {
            res.status(400).send("All input is required");
        }

        // check if city already exist
        const existCity = await City.findOne({ cityName });

        if (existCity) {
            return res.status(409).send("City Already Exist.");
        }


        const city = await City.create({
            cityName,
        });

        // return new user
        res.status(201).json(city);
    } catch (err) {
        console.log(err);
    }
}
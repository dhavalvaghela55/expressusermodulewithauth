
const User = require("../../../model/driver");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
exports.delete = async (req, res) => {
    // Our login logic starts here
    try {
        // Get user input
        const { email } = req.body;

        // Validate user input
        if (!(email)) {
            res.status(400).send("All input is required");
        }
        // Validate if user exist in our database
        const user = await User.deleteOne({ email });
        console.log('user',user)
        if (user.deletedCount > 0) {
            res.status(200).send({ "data": user });
        } else {
            res.status(400).send("email not found");
        }
    } catch (err) {
        console.log(err);
    }
    // Our register logic ends here
}
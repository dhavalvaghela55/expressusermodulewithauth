const express = require("express");
const router = express.Router();
const user = require("./delete");
const auth = require("../../../middleware/auth");

router.delete("/driver", auth, user.delete);


module.exports = router;

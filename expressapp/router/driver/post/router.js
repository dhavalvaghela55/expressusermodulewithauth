const express = require("express");
const router = express.Router();
const register = require("./register");
const signIn = require("./login");

router.post("/driver/register", register.create);

router.post("/driver/login", signIn.login);

module.exports = router;

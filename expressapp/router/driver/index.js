'use strict'

const get = require('./get');

const post = require('./post');

const deleteUser = require('./delete');

module.exports = [].concat(
    get,
    post,
    deleteUser
);

const User = require("../../../model/driver");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
exports.getUser = async (req, res) => {

    // Our register logic starts here
    try {

        // Get user input
        // const { email } = req.query;

        // Validate user input
        // if (!(email)) {
        //     res.status(400).send("All input is required");
        // }

        // check if user already exist
        // Validate if user exist in our database


        if (req.user) {
            let email = req.user.email;
            const user = await User.findOne({ email });
            console.log('user',user)
            console.log('user',user.task)
            res.status(201).json({ 'firstName': user.first_name, 'lastName': user.last_name, 'email': email, 'task': user.task });

        } else {
            return res.status(409).send("No Users Found");
        }

    } catch (err) {
        console.log(err);
    }
}
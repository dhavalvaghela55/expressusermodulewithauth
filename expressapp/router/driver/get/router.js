const express = require("express");
const router = express.Router();
const users = require("./getAllUser");
const user = require("./get");
const auth = require("../../../middleware/auth");

router.get("/driver/user", auth, users.getAllUser)
router.get("/driver/getUser", auth, user.getUser)
module.exports = router;

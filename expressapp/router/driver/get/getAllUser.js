
const User = require("../../../model/driver");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
exports.getAllUser = async (req, res) => {
    // Our register logic starts here
    try {

        // Get user input
        const { skip, limit } = req.query;

        // Validate user input
        if (!(skip && limit)) {
            res.status(400).send("All input is required");
        }

        console.log('skip',skip)
        console.log('limit',limit)
        // check if user already exist
        // Validate if user exist in our database
        const Users = await User.find().sort().skip(parseInt(skip)).limit(parseInt(limit))
        
        if (Users) {
            res.status(201).json(Users);
          
        }else{
            return res.status(409).send("No Users Found");
        }
      
    } catch (err) {
        console.log(err);
    }
}
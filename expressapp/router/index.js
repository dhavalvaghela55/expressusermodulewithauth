'use strict';

const driver = require('./driver');
const admin = require('./admin');

module.exports = [].concat(
    driver,
    admin
);

const stub = require('../../../stub');
const apiHandler = require('../../../.././router/user/post/login');
const db = require('./../../../db');
const axios = require('axios');


beforeAll(async () => {
    await db.connect();
});

afterAll(async () => {
    await db.closeDatabase();
});


describe('User signin api', () => {
    it('should call user signin API -200', async (done) => {
        const req = { ...stub.req }

        let payload = { ...stub.customerSigninStub };
       
        req.body = payload;
        console.log('req.body',req.body)
        axios
        .post('http://localhost:5000/app/login', req.body)
        .then(res => {
            console.log('status--->',res.status);
            expect(res.data).toBeDefined();
            expect(res.status).toBeGreaterThanOrEqual(200);
            expect(res.status).toBeLessThan(300);
            done()
        })
        .catch(error => {

            // expect(error.response.status).toBeLessThan();
            expect(error.response.status).toBeUndefined();
            // expect(error.response.status).toBeLessThan(500);
            done();
            // return;
            // expect(error.response.status).toBeGreaterThanOrEqual(400);
            // expect(error.response.status).toBeLessThan(500);
        })

        
    })// end  customer signin api -200



});//end customer signin api



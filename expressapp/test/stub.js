
const { user } = require('./models/user');


const headerAuthorization = JSON.stringify({ userId: '61306aa99bcd4a7fd776e007', userType: 'user', })

const req = {
  headers: {
    authorization: headerAuthorization,
  },
  payload: {},
  query: {},
  params: {}
}
const limit = 1
const skip = 0
const sortField = "_id"
const sortOrder = 1



const customerSigninStub = {
  email: user[0].email,
  password: "12345678",

}

module.exports = {
  req,
  headerAuthorization,
  customerSigninStub,
  limit,
  skip,
  sortField,
  sortOrder,
}

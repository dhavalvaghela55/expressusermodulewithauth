// const ObjectId = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;

module.exports.user = [
    {
        "_id": "61306aa99bcd4a7fd776e007",
        "first_name": "dv",
        "last_name": "last_name",
        "email": "dhaval@test.com",
        "password": "$2b$10$OmvlfCnxiskRXkmK7NHXgexshxR78rnek925GMJhwchSi7Jl/96Ne",
        "task": [],
    },//correct user

    {
        "_id": "612e3db785e96cc9cf02901c",
        "first_name": "tesst",
        "last_name": "usser",
        "email": "test@tsest.com",
        "password": "$2b$10$29bOm2fbxTm1DCN3xWXEHOizo3ZAZfZ2.zXHWn.mULFNHilPHY5Wm",
        "task": [],
    }
]
const mongoose = require("mongoose");

const  MONGO_URI  = "mongodb://appUsers:appUsers@localhost:27017/userDb";
console.log('MONGO_URI',MONGO_URI)
exports.connect = async () => {
  // Connecting to the database
  await mongoose
    .connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    //   useCreateIndex: true,
    //   useFindAndModify: false,
    })

   
    .then(() => {
      console.log("Successfully connected to database");
    })
    .catch((error) => {
      console.log("database connection failed. exiting now...");
      console.error(error);
      process.exit(1);
    });
};

exports.closeDatabase = async () => {
   
   await mongoose.connection.dropDatabase()
   await mongoose.connection.close();
  };

  exports.clearDatabase = async () => {
      console.log('clearDatabase')
      const collections = await mongoose.connection.db.collections()

      for (let collection of collections) {
        await collection.remove()
      }
  };
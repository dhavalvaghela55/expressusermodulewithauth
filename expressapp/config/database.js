const mongoose = require("mongoose");

// const  MONGO_URI  ="mongodb://appUsers:appUsers@localhost:27017/userDb";
const  MONGO_URI  ="mongodb+srv://3Embed007:3Embed007@cluster0.3igjnx2.mongodb.net/?retryWrites=true&w=majority";
console.log('MONGO_URI',MONGO_URI)
exports.connect = () => {
  // Connecting to the database
  mongoose
    .connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    //   useCreateIndex: true,
    //   useFindAndModify: false,
    })
    .then(() => {
      console.log("Successfully connected to database");
    })
    .catch((error) => {
      console.log("database connection failed. exiting now...");
      console.error(error);
      process.exit(1);
    });
};